/******************************************************************************
 * Copyright (c) 2000-2023 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 * Michael Josenhans
 ******************************************************************************/
//
//  File:               SocketCAN_Types.ttcn
//  Description:        SocketCAN definition file
//
// Revision R1A

module SocketCAN_Types
{    
  import from Bcm all
  import from Can all
  import from Raw all
  import from J1939 all
 
const integer J1939_FILTER_MAX := 512
  
type integer             SocketCAN_socketid;
type charstring          SocketCAN_if_name;
type integer             SocketCAN_if_index (-32767 .. +32767); 

type octetstring SocketCAN_Isotp_PDU
type octetstring SocketCAN_J1939_PDU length(0..117440505); // (2^24-1)*7= 117440505

type record SocketCAN_timeval
{
   integer tv_sec,		    // Seconds
   integer tv_usec	        // Microseconds
}

type union SocketCAN_CAN_or_CAN_FD_frame {
  CAN_frame                        can_frame,
  CANFD_frame                      canfd_frame
}

type record SocketCAN_ifr {
  SocketCAN_if_name                if_name optional, 
  SocketCAN_if_index               if_index optional
} 

type enumerated SocketCAN_if_any { ANY_INTERFACE}

type union SocketCAN_send_data_ifu {
  SocketCAN_if_name                if_name, 
  SocketCAN_if_index               if_index,
  SocketCAN_if_any                 if_any
} 

type union SocketCAN_ioctl_ifu {
  SocketCAN_if_name                if_name, 
  SocketCAN_if_index               if_index,
  SocketCAN_if_any                 if_any
} 

type enumerated SocketCAN_Result_code 
{ 
  SocketCAN_ERROR (0),
  SocketCAN_SUCCESS(1)
}

type record SocketCAN_Result{
  SocketCAN_Result_code            result_code,
  integer                          err  (-32767 .. +32767) optional,
  charstring                       err_text optional
}

type record SocketCAN_socket{
  AdresseFamily_enum               domain,
  ProtocolFamily_enum              ptype,
  PF_CAN_protocols_enum            protocol
}

type record SocketCAN_socket_result{
  SocketCAN_socketid               id,
  SocketCAN_Result                 result
}

type record SocketCAN_ioctl{
  SocketCAN_socketid               id,
  SocketCAN_ioctl_ifu              ifu optional
}

type record SocketCAN_ioctl_result{
  SocketCAN_ifr                    ifr,
  SocketCAN_Result                 result
}

type union SocketCAN_connectu{
  SocketCAN_connect_bcm     bcm,
  SocketCAN_connect_j1939   j1939
}

type record SocketCAN_connect_bcm {
  SocketCAN_if_index               if_index
}

type record SocketCAN_connect_j1939 {
  SocketCAN_if_index               if_index,
  J1939_hdr                        j1939_destination
}

type record SocketCAN_connect{
  SocketCAN_socketid               id,
  SocketCAN_connectu               connectu
}

type record SocketCAN_connect_result{
  SocketCAN_Result                 result
}

type record SocketCAN_bind_raw {
  SocketCAN_if_index               if_index
}

type record SocketCAN_bind_isotp {
  SocketCAN_if_index               if_index,
  CAN_id                           rx_can_id,
  CAN_id                           tx_can_id
}

type record SocketCAN_bind_j1939 {
  SocketCAN_if_index               if_index,
  J1939_hdr                        j1939_source
}

type union SocketCAN_bindu{
  SocketCAN_bind_raw   raw,
  SocketCAN_bind_isotp isotp,
  SocketCAN_bind_j1939 j1939
}

type record SocketCAN_bind{
  SocketCAN_socketid               id,
  SocketCAN_bindu                  bindu
}

type record SocketCAN_bind_result{
  SocketCAN_Result                 result
}

type record SocketCAN_send_data{
  SocketCAN_socketid               id,
  // if the following optional parameter is not provided, the CAN interface name is taken from
  // the TTCN configuration file
  SocketCAN_send_data_ifu          ifu optional,
  SocketCAN_CAN_or_CAN_FD_frame    frame
}

type record SocketCAN_send_data_result{
  SocketCAN_Result                 result
}

type record SocketCAN_j1939_send_data{
  SocketCAN_socketid               id,
  SocketCAN_J1939_PDU              pdu
}

type record SocketCAN_j1939_send_data_result{
  SocketCAN_Result                 result
}

type record SocketCAN_j1939_send_data_to{
  SocketCAN_socketid               id,
  SocketCAN_if_index               if_index,
  J1939_hdr                        j1939_destination,
  SocketCAN_J1939_PDU              pdu
}

type record SocketCAN_j1939_send_data_to_result{
  SocketCAN_Result                 result
}

type record SocketCAN_write_data{
  SocketCAN_socketid               id,
  SocketCAN_bcm_frame              bcm_tx_msg
}

type record SocketCAN_write_data_result{
  SocketCAN_Result                 result
}

type record SocketCAN_write_isotp{
  SocketCAN_socketid               id,
  SocketCAN_Isotp_PDU              pdu
}

type record SocketCAN_write_isotp_result{
  SocketCAN_Result                 result
}

type record SocketCAN_receive_CAN_or_CAN_FD_frame{ // recieved CAN or CAN FD frame
  SocketCAN_socketid               id,
  SocketCAN_ifr                    ifr,
  SocketCAN_CAN_or_CAN_FD_frame    frame, 
  SocketCAN_timeval                timestamp
}

type record SocketCAN_receive_BCM_message { // recieved CAN BCM message
  SocketCAN_socketid               id,
  SocketCAN_ifr                    ifr,
  SocketCAN_bcm_frame              frame
}

type record SocketCAN_receive_isotp_pdu { // recieved Isotp PDU
  SocketCAN_socketid               id,
  SocketCAN_ifr                    ifr,
  SocketCAN_Isotp_PDU              pdu
}

type record SocketCAN_receive_j1939_message { // recieved J1939 message
  SocketCAN_socketid               id,
  SocketCAN_ifr                    ifr,
  J1939_PGN                        pgn,
  J1939_ADDR                       destAddr,
  J1939_NAME                       name,
  SocketCAN_J1939_PDU              pdu
}

type record length (0 .. 255) of CAN_RAW_filter CAN_RAW_filters
type record length (0 .. J1939_FILTER_MAX) of J1939_filter   J1939_filters 
 
type record J1939_NAME_filter {
  J1939_NAME                      name,
  J1939_NAME                      name_mask
} 

type record J1939_ADDR_filter {
  J1939_ADDR                      addr,
  J1939_ADDR                      addr_mask
} 

type record J1939_PGN_filter { 
  J1939_PGN                       pgn,
  J1939_PGN                       pgn_mask
} 

type record J1939_filter {
  J1939_NAME_filter                      name_filter       optional,
  J1939_ADDR_filter                      addr_filter       optional,
  J1939_PGN_filter                       pgn_filter        optional
} 

type union SocketCAN_setsockopt_commandu {
  CAN_RAW_filters                  rfilter,  
  CAN_RAW_err_mask                 err_mask,
  CAN_RAW_loopback_enum            loopback,
  CAN_RAW_recv_own_msgs_enum       recv_own_msgs, 
  CAN_RAW_fd_frames_enum           fd_frames,
  CAN_RAW_join_filters_enum        join_filters,
  J1939_filters                    j1939_filter,
  J1939_PROMISC_enum               j1939_promisc,
  J1939_Priority                   j1939_prio,
  J1939_Broadcast_enum             j1939_broadcast
}

type record SocketCAN_setsockopt{
  SocketCAN_socketid               id,
  SocketCAN_setsockopt_commandu    command
}

type record SocketCAN_setsockopt_result{
  SocketCAN_Result                 result
}

type record SocketCAN_close{
  SocketCAN_socketid               id
}

type enumerated DisableEnable_enum {
  Disable      (0),
  Enable       (1)
}

type DisableEnable_enum      CAN_RAW_loopback_enum;
type DisableEnable_enum      CAN_RAW_recv_own_msgs_enum;
type DisableEnable_enum      CAN_RAW_fd_frames_enum;
type DisableEnable_enum      CAN_RAW_join_filters_enum;
type DisableEnable_enum      J1939_PROMISC_enum;
type DisableEnable_enum      J1939_Broadcast_enum;
}
